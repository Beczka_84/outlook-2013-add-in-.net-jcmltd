﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAddin
{
    public class Constants
    {
        public static string Positive = "Positive";
        public static string Negative = "Negative";
        public static string CantDetermine = "Can't determine";

    }
}
