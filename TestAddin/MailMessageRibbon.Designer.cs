﻿namespace WebServAddin
{
    partial class MailMessageRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public MailMessageRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.txtResult = this.Factory.CreateRibbonEditBox();
            this.cbRealTimeAnalysis = this.Factory.CreateRibbonCheckBox();
            this.btnAnalyse = this.Factory.CreateRibbonButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.ControlId.OfficeId = "TabNewMailMessage";
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabNewMailMessage";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.txtResult);
            this.group1.Items.Add(this.cbRealTimeAnalysis);
            this.group1.Items.Add(this.btnAnalyse);
            this.group1.Name = "group1";
            // 
            // txtResult
            // 
            this.txtResult.Label = "Social Opinion :";
            this.txtResult.Name = "txtResult";
            this.txtResult.SizeString = "11111111111111111111";
            this.txtResult.Text = null;
            // 
            // cbRealTimeAnalysis
            // 
            this.cbRealTimeAnalysis.Label = "Real-time analysis";
            this.cbRealTimeAnalysis.Name = "cbRealTimeAnalysis";
            this.cbRealTimeAnalysis.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbRealTimeAnalysis_Click);
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Label = "Analyse";
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Visible = false;
            this.btnAnalyse.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAnalyse_Click);
            // 
            // MailMessageRibbon
            // 
            this.Name = "MailMessageRibbon";
            this.RibbonType = "Microsoft.Outlook.Mail.Compose";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.AddinRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonCheckBox cbRealTimeAnalysis;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAnalyse;
        private System.Windows.Forms.Timer timer;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox txtResult;
    }

    partial class ThisRibbonCollection
    {
        internal MailMessageRibbon MailMessageRibbon
        {
            get { return this.GetRibbon<MailMessageRibbon>(); }
        }
    }
}
