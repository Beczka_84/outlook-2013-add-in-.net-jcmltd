﻿namespace WebServAddin
{
    partial class Panel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbRealTimeAnalysis = new System.Windows.Forms.CheckBox();
            this.btnAnalyse = new System.Windows.Forms.Button();
            this.lblSocial = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lblResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbRealTimeAnalysis
            // 
            this.cbRealTimeAnalysis.AutoSize = true;
            this.cbRealTimeAnalysis.Location = new System.Drawing.Point(3, 101);
            this.cbRealTimeAnalysis.Name = "cbRealTimeAnalysis";
            this.cbRealTimeAnalysis.Size = new System.Drawing.Size(110, 17);
            this.cbRealTimeAnalysis.TabIndex = 0;
            this.cbRealTimeAnalysis.Text = "Real-time analysis";
            this.cbRealTimeAnalysis.UseVisualStyleBackColor = true;
            this.cbRealTimeAnalysis.CheckedChanged += new System.EventHandler(this.cbRealTimeAnalysis_CheckedChanged);
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Location = new System.Drawing.Point(3, 124);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(75, 23);
            this.btnAnalyse.TabIndex = 1;
            this.btnAnalyse.Text = "Analyse";
            this.btnAnalyse.UseVisualStyleBackColor = true;
            this.btnAnalyse.Click += new System.EventHandler(this.btnAnalyse_Click);
            // 
            // lblSocial
            // 
            this.lblSocial.AutoSize = true;
            this.lblSocial.Location = new System.Drawing.Point(12, 7);
            this.lblSocial.Name = "lblSocial";
            this.lblSocial.Size = new System.Drawing.Size(78, 13);
            this.lblSocial.TabIndex = 2;
            this.lblSocial.Text = "Social Opinion:";
            // 
            // lblResult
            // 
            this.lblResult.Location = new System.Drawing.Point(15, 23);
            this.lblResult.Multiline = true;
            this.lblResult.Name = "lblResult";
            this.lblResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.lblResult.Size = new System.Drawing.Size(182, 28);
            this.lblResult.TabIndex = 4;
            // 
            // Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblSocial);
            this.Controls.Add(this.btnAnalyse);
            this.Controls.Add(this.cbRealTimeAnalysis);
            this.Name = "Panel";
            this.Size = new System.Drawing.Size(212, 150);
            this.Load += new System.EventHandler(this.Panel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.CheckBox cbRealTimeAnalysis;
        private System.Windows.Forms.Button btnAnalyse;
        private System.Windows.Forms.Label lblSocial;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox lblResult;
    }
}
