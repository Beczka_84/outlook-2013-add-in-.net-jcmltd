﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Threading.Tasks;
using TestAddin;

namespace WebServAddin
{
    public partial class MailMessageRibbon
    {
        CoreService service;
        private void AddinRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            this.cbRealTimeAnalysis.Checked = true;

            this.timer.Interval = 5000;
            this.timer.Tick += Timer_Tick;
            this.timer.Start();

            service = new CoreService();

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Task.Run(() => DoJob());
        }

        private void cbRealTimeAnalysis_Click(object sender, RibbonControlEventArgs e)
        {
            if (this.btnAnalyse.Visible)
            {
                this.btnAnalyse.Visible = false;
                this.timer.Start();
            }
            else
            {
                this.btnAnalyse.Visible = true;
                this.timer.Stop();
            }
        }

        private void DoJob()
        {
            string webApiresponse;
            try
            {
                var response = service.GetSentiment(GlobalMail._item.Body);

                if (response.Length > 20)
                {
                    webApiresponse = response.Substring(0, 20);
                }
                else {
                    webApiresponse = response;
                };
                this.txtResult.Text = webApiresponse;
                if (webApiresponse.Contains(Constants.CantDetermine)) this.txtResult.OfficeImageId = "Risks";
                if (webApiresponse.Contains(Constants.Positive)) this.txtResult.OfficeImageId = "AcceptInvitation";
                if (webApiresponse.Contains(Constants.Negative)) this.txtResult.OfficeImageId = "DeclineInvitation";

                this.txtResult.ShowImage = true;
            }
            catch (Exception e)
            {
                this.txtResult.Text = "Error";
            }
        }

        private void btnAnalyse_Click(object sender, RibbonControlEventArgs e)
        {
            Task.Run(() => DoJob());
        }

    }
}
