﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServAddin
{
    internal class MailItemWrapper : InspectorWrapper
    {

        public MailItemWrapper(Microsoft.Office.Interop.Outlook.Inspector inspector)
            : base(inspector)
        {
        }

        public Microsoft.Office.Interop.Outlook.MailItem Item { get; private set; }

        protected override void Initialize()
        {
            Item = (Microsoft.Office.Interop.Outlook.MailItem)Inspector.CurrentItem;
            if (Inspector != null)
            {
                ((InspectorEvents_10_Event)Inspector).Close += Inspector_Close;
            }

            Item.Read += Item_Read;
            Item.Open += Item_Open;

        }

        private void Item_Open(ref bool Cancel)
        {
            if (GlobalMail._item != null)
                GlobalMail._previousItem = GlobalMail._item;

            GlobalMail._item = Item;

            GlobalMail._counter++;

        }

        private void Item_Read()
        {
            GlobalMail._item = Item;

        }

        private void Inspector_Close()
        {
            if (GlobalMail._previousItem != null)
                GlobalMail._item = GlobalMail._previousItem;

            GlobalMail._counter--;
        }



        protected override void Close()
        {
            Item.Read -= Item_Read;
            Item.Open -= Item_Open;
            Item = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
