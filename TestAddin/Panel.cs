﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestAddin;
//

namespace WebServAddin
{
    public partial class Panel : UserControl
    {
        CoreService service;

        public Panel()
        {
            InitializeComponent();

            this.lblResult.Hide();
            this.lblResult.Text = "";
            this.cbRealTimeAnalysis.Checked = true;

            this.timer.Interval = 5000;
            this.timer.Tick += Timer_Tick;
            this.timer.Start();

            service = new CoreService();

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Task.Run(() => DoJob());
        }

        private void DoJob()
        {
            string webApiresponse;
            try { 
                var response = service.GetSentiment(GlobalMail._item.Body);
                if (response.Length > 20)
                {
                    webApiresponse = response.Substring(0, 20);
                }
                else
                {
                    webApiresponse = response;
                };

            this.lblResult.Invoke(new MethodInvoker(delegate () { ShowResponse(webApiresponse); }));
            }
            catch (Exception e)
            {
                this.lblResult.Invoke(new MethodInvoker(delegate () { ShowResponse("Error"); }));
            }

        }

        private void ShowResponse(string value)
        {
            if (GlobalMail._item != null)
            {

                if (GlobalMail._counter == 0)
                {
                    this.lblResult.Show();
                    this.lblResult.Text = value;
                    if (value.Contains(Constants.CantDetermine)) this.lblResult.BackColor = Color.Orange;
                    if (value.Contains(Constants.Positive)) this.lblResult.BackColor = Color.Green;
                    if (value.Contains(Constants.Negative)) this.lblResult.BackColor = Color.Red;
                }
            }
        }

        private void btnAnalyse_Click(object sender, EventArgs e)
        {
            Task.Run(() => DoJob());
        }

        private void cbRealTimeAnalysis_CheckedChanged(object sender, EventArgs e)
        {
            if (this.btnAnalyse.Visible)
            {
                this.btnAnalyse.Visible = false;
                this.timer.Start();
            }
            else
            {
                this.btnAnalyse.Visible = true;
                this.timer.Stop();
            }
        }

        private void Panel_Load(object sender, EventArgs e)
        {

        }
    }
}
